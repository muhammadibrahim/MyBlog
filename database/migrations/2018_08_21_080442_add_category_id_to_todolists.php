<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToTodolists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('blogs', function(Blueprint $table)
      {
          $table->integer('categorys_id')->unsigned()->after('id');
          $table->foreign('categorys_id')->references('id')->on('categorys');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('blogs', function (Blueprint $table) {
            //
        });
    }
}
