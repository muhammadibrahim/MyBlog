<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;


class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $blog = Blog::all();
      return view('site.index',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $blog = new Blog;
      $kategori = Category::lists('name_category','id');

      return view('site.create',compact('blog','kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $blog = new Blog;
      $blog->categorys_id = $request->categorys_id;
      $blog->title = $request->title;
      $blog->content = $request->content;

      $blog->save();
      \Session::flash('flash_message','data berhasil di simpan');
      return redirect()->action('SiteController@index');
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $blog = Blog::find($id);
      $kategori = Category::lists('name_category','id');

      return view('site.create',compact('blog','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $blog = Blog::find($id);
      $blog->categorys_id = $request->categorys_id;
      $blog->title = $request->title;
      $blog->content = $request->content;

      $blog->save();
      \Session::flash('flash_message','data berhasil di update');

      return redirect()->action('SiteController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $blog = Blog::destroy($id);
      \Session::flash('flash_message','data berhasil di hapus');
      return redirect()->action('SiteController@index');
    }
}
