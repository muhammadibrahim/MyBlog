<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class PageBlogController extends Controller
{
  public function index()
  {
    $blogs = Blog::orderBy('id', 'DESC')->paginate(5);
    return view('pageblog.index', compact('blogs'));
  }

  public function show($id)
  {
    $blog = Blog::findOrFail($id);
    return view('pageblog.show', compact('blog'));
  }

}
