@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Artikel</div>

                <div class="card-body">
                  <h4>{{ $blog->title }}</h4>
                  <p>{{ $blog->content }}</p>
                  <a href="{{ route('blog.index') }}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
