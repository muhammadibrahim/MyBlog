@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Artikel</div>

                <div class="card-body">
                  <a href="{{ route('blog.create') }}" class="btn btn-primary">Artikel Baru</a>
                  <br><br>

                  @if ($message = Session::get('message'))
                      <div class="alert alert-success martop-sm">
                          <p>{{ $message }}</p>
                      </div>
                  @endif
                    <table class="table table-hover border">
                      <thead style="background:#a8c6f7">
                          <th>ID</th>
                          <th style="width:70%">Title</th>
                          <th>Action</th>
                      </thead>
                      <tbody>
                          {{-- buat nomor urut --}}
                          @php($no = 1)
                          {{-- loop all --}}
                          @foreach ($blogs as $blog)
                              <tr>
                                  <td>{{ $no++ }}</td>
                                  <td>{{ $blog->title }}</td>
                                  <td>
                                      <form action="{{ route('blog.destroy', $blog->id) }}" method="post">
                                          {{csrf_field()}}
                                          {{ method_field('DELETE') }}
                                          <a href="{{ route('blog.show', $blog->id) }}" class="btn btn-success btn-sm">View</a>
                                          <a href="{{ route('blog.edit', $blog->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                                          <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                      </form>
                                  </td>
                              </tr>
                          @endforeach
                          {{-- // end loop --}}
                      </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
