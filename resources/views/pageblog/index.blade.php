@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  @foreach ($blogs as $blog)
                      <div class="row justify-content-center">
                        <div class="col-md-11">
                          <h3>{{ $blog->title }}</h3>
                          <small><i>{{ $blog->created_at }}</i></small>
                          <p>{!!  substr(strip_tags($blog->content), 0, 400) !!} ...</p>
                          <p><a class="btn btn-primary" href="{{ route('pageblog.show', $blog->id) }}" role="button">Read More &raquo;</a></p>
                          <hr>
                        </div>
                      </div>

                  @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
