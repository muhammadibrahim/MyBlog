@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Artikel</div>

                <div class="card-body">
                  <div class="row justify-content-center">
                    <div class="col-md-11">
                      <h3>{{ $blog->title }}</h3>
                      <small><i>{{ $blog->created_at }}</i></small>
                      <p>{{ $blog->content }}</p>
                      <a href="{{ route('pageblog.index') }}" class="btn btn-default">Kembali</a>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
