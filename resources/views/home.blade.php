@extends('layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <?php if (Auth::user()->role_id == 1): ?>
                        <div class="col-md-12">
                          <div class="jumbotron">
                            <center>
                              <h1>Welcome</h1>
                              <p>You are login as <b>Admin</b></p>
                              <p>
                                <a class="btn btn-default btn-primary" href="{{ url('pageblog') }}" role="button"> View Blog </a>
                                <a class="btn btn-default btn-success" href="{{ route('user.create') }}" role="button"> create User </a>
                              </p>
                            </center>
                          </div>
                        </div>

                    <?php else: ?>
                      <div class="col-md-12">
                        <div class="jumbotron">
                          <center>
                            <h1>Welcome</h1>
                            <p>You are login as <b>Author</b></p>
                            <p>
                              <a class="btn btn-default btn-primary" href="{{ url('blog') }}" role="button"> View Blog </a>
                            </p>
                          </center>
                        </div>
                      </div>
                    <?php endif; ?>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
