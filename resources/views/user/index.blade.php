@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data User</div>

                <div class="card-body">
                  <a href="{{ route('user.create') }}" class="btn btn-primary btn-default">User Baru</a>
                  <br><br>

                  @if ($message = Session::get('message'))
                      <div class="alert alert-success martop-sm">
                          <p>{{ $message }}</p>
                      </div>
                  @endif

                  <table class="table table-hover">
                      <thead style="background:#a8c6f7">
                          <th>ID</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Level</th>
                          <th>Action</th>
                      </thead>
                      <tbody>
                          {{-- buat nomor urut --}}
                          @php($no = 1)
                          {{-- loop all --}}
                          @foreach ($users as $user)
                              <tr>
                                  <td>{{ $no++ }}</td>
                                  <td><a href="{{ route('user.show', $user->id) }}">{{ $user->name }}</a></td>
                                  <td>{{ $user->email }}</td>
                                  <?php if ($user-> role_id==1): ?>
                                    <td>Admin</td>
                                  <?php else: ?>
                                    <td>Author</td>
                                  <?php endif; ?>
                                  <td>
                                      <form action="{{ route('user.destroy', $user->id) }}" method="post">
                                          {{csrf_field()}}
                                          {{ method_field('DELETE') }}
                                          <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                                          <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                      </form>
                                  </td>
                              </tr>
                          @endforeach
                          {{-- // end loop --}}
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
